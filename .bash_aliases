alias vup="vagrant up"
alias vhalt="vagrant halt"
alias vssh="ssh -i ~/.vagrant.d/insecure_private_key vagrant@localhost -p 2222"

alias vdup="cd ~/Vagrant/Development; vagrant up"
alias vdhalt="cd ~/Vagrant/Development; vagrant halt"
alias vdssh="ssh -i ~/.vagrant.d/insecure_private_key vagrant@localhost -p 2222"

alias ls='ls -liah --color=auto'
alias vi=vim
alias edit='vim'

alias gpush="git push origin master"
alias gpull="git pull origin master"
alias gfetch"git fetch --all"

# do not delete / or prompt if deleting more than 3 files at a time #
alias rm='rm -I --preserve-root'
alias su='sudo -i'
